package com.geektechnique.gsonpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GsonPracticeApplication {

    public static void main(String[] args) {
        SpringApplication.run(GsonPracticeApplication.class, args);
    }

}
